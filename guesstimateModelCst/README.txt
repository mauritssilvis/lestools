DESCRIPTION

Obtain an estimate of the constant of an eddy viscosity model for large-
eddy simulation of turbulent flows by requiring that its average 
dissipation matches the dissipation of the Smagorinsky model for a large 
number of random velocity gradients, sampled from a uniform or a Gaussian 
distribution.

This is implemented by computing the ratio of the estimated average 
(scaled) dissipation due to the Smagorinsky model and the estimated 
average (scaled) dissipation of the model of interest, where averages are
taken over a large number of random velocity gradients. As a result one 
obtains an estimate of the value of the model constant, c_1, in terms of 
the Smagorinsky model constant (squared), C_S^2.

Note that the scaled dissipation for the Smagorinsky model is defined as
sqrt(2*I1) * I1 / C_S^2 / delta^2, where sqrt(2*I1) is called the scaled 
eddy viscosity. Here, I1 is the second invariant of the rate-of-strain
tensor and delta is the large-eddy simulation filter length, usually 
associated with the grid spacing. In this script, delta is assumed to be 
implemented in the same fashion in the Smagorinsky model and the model of
interest, so it can be divided out in the ratio of the average
dissipations.

EXAMPLE CALLS

guesstimateModelCst('Vreman', 'sqrt( ( 1/4*(I1 + I2)^2 + 4*(I5 - 1/2*I1*I2) ) / (I1 - I2) )', 0.05)
meaning modName = 'Vreman',
        modVisc = 'sqrt( ( 1/4*(I1 + I2)^2 + 4*(I5 - 1/2*I1*I2) ) / (I1 - I2) )',
        minRelPrecision = 0.05,
        msgMode = 1 (default message mode)
        
guesstimateModelCst('QR', '2/3/3*max(-I3, 0)/I1', 0.01, 2)
meaning modName = 'QR',
        modVisc = '2/3/3*max(-I3, 0)/I1',
        minRelPrecision = 0.05,
        msgMode = 2 (verbose message mode)
        
MORE INFORMATION

type 'help guesstimateModelCst' for more information

ACKNOWLEDGMENTS

Mirko Signorelli is kindly acknowledged for his assistance in determining
the accuracy of the model constant estimates.

LICENSE

Copyright (c) 2016 Maurits H. Silvis

This file is subject to the terms and conditions defined in 
the MIT License, which can be found in the file 'LICENSE.txt' 
that is part of this source code package.