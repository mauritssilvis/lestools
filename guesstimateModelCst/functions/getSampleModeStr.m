function sampleModeStr = getSampleModeStr(sampleMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Auxiliary function that turns the int sampleMode into the corresponding
% name of the sampling mode
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sampleMode:       int - sampling mode
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sampleModeStr:    string - sampling mode
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (c) 2016 Maurits H. Silvis
%
% This file is subject to the terms and conditions defined in 
% the MIT License, which can be found in the file 'LICENSE.txt' 
% that is part of this source code package.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Check which sampling mode is used and return the corresponding name
    if sampleMode == 0
        sampleModeStr = 'uniform';
    elseif sampleMode == 1
        sampleModeStr = 'Gaussian';
    end
    
end