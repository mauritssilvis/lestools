function [modDissAvg, modDissRelStdDev] = obtainModDissStats(modName, modVisc, sampleMode, minRelPrecision, msgMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Given a scaled eddy viscosity model, obtain an estimate of its scaled 
% dissipation and the corresponding precision. This script first tries to 
% load the requested data from a database. If the data is not available, it
% will be constructed (and stored).
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modName:          string - name of eddy viscosity model
% modVisc:          string - scaled eddy viscosity
% sampleMode:       int - sampling mode
%                       0: uniform sampling,
%                       1: Gaussian sampling
% minRelPrecision:  double - desired minimal relative standard deviation
% msgMode:          int - switch that controls the output
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modDissAvg:       double - estimate of the model dissipation
% modDissRelStdDev: double - estimate of its relative error
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (c) 2016 Maurits H. Silvis
%
% This file is subject to the terms and conditions defined in 
% the MIT License, which can be found in the file 'LICENSE.txt' 
% that is part of this source code package.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Obtain an estimate of the scaled model dissipation and its precision
dispMsg(['   Estimate the scaled model dissipation and its precision for:'], 2, msgMode);
dispMsg(['   Model: ', modName], 2, msgMode);
dispMsg(['   Sample mode: ', getSampleModeStr(sampleMode)], 2, msgMode);
dispMsg(['   Minimum desired relative standard deviation: minRelPrecision / sqrt(2) = ', num2str(minRelPrecision / sqrt(2))], 2, msgMode);

% Attempt to load the requested data from a database
dispMsg(' - Attempt to load the requested data from a database', 1, msgMode);

% Check if the database file exists
dispMsg('   - Check if the database (dat/ModDissStatsDB.mat) exists', 2, msgMode);

% Set output padding depending on the msgMode
msgPad = blanks( (msgMode - 1)*2 );

if exist('dat/modDissStatsDB.mat', 'file') == 2
    % The data file exist
    dispMsg([msgPad, '   - The database exists'], 1, msgMode);
    load('dat/modDissStatsDB', 'modDissStats')
    dataFileExists = true;
    
    % Check if the data file contains the requested data
    dispMsg('   - Check if the database contains the requested data', 2, msgMode);

    % Order the data so that results with the highest precision are found
    % first
    dispMsg('     - Order the data by increasing relative standard deviation', 2, msgMode);
    [~,index] = sortrows({modDissStats.modDissStdDevRel}.'); modDissStats = modDissStats(index); clear index

    reqDataExists = false;
    availData = [0 0];
    availDataCount = 0;
    for idxExp = 1 : numel(modDissStats)
        dispMsg(['     - Check if experiment ', int2str(idxExp), ' contains the requested data'], 2, msgMode);
        curModName = modDissStats(idxExp).modName;
        curSampleMode = modDissStats(idxExp).sampleMode;
        curStdDevRel = modDissStats(idxExp).modDissStdDevRel;
        
        if strcmp(curModName, modName) == true && curSampleMode == sampleMode
            % The database contains data for the model and sampling mode of
            % interest
            dispMsg(['       - Experiment ', int2str(idxExp), ' contains data for the model and sampling mode of interest'], 2, msgMode);
            
            % Store which data is available so that recomputation can be
            % avoided when attempting to create additional data when needed
            curNumSamples = modDissStats(idxExp).numSamples;
            curNumExpsPerSample = modDissStats(idxExp).numExpsPerSample;
            availDataCount = availDataCount + 1;
            availData(availDataCount, :) = [curNumSamples curNumExpsPerSample];
            
            if curStdDevRel < minRelPrecision / sqrt(2)
                % The database contains the requested data
                dispMsg(['       - Experiment ', int2str(idxExp), ' contains data that is of the desired precision: ', num2str(curStdDevRel), ' < minRelPrecision / sqrt(2) = ', num2str(minRelPrecision / sqrt(2))], 2, msgMode);
                reqDataExists = true;

                modDissAvg = modDissStats(idxExp).modDissAvg;
                modDissRelStdDev = curStdDevRel;
                break;
            else
                % The current experiment does not contain the requested data
                dispMsg(['       - Experiment ', int2str(idxExp), ' does not contain data that is of the desired precision as ', num2str(curStdDevRel), ' >= minRelPrecision / sqrt(2) = ', num2str(minRelPrecision / sqrt(2))], 2, msgMode);
            end
        else
            % The current experiment does not contain data for the model
            % and sampling mode of interest
            dispMsg(['       - Experiment ', int2str(idxExp), ' does not contain data for the model or sampling mode of interest'], 2, msgMode);
        end
    end
    
    if reqDataExists
        % The database contains the requested data
        dispMsg([msgPad, '   - The database contains the requested data'], 1, msgMode);
    else
        % The database does not contain the requested data
        dispMsg([msgPad, '   - The database does not contain the requested data'], 1, msgMode);        
    end
    
else
    % The database file does not exist 
    dispMsg([msgPad, '   - The database doesn''t exists'], 1, msgMode);
    dataFileExists = false;
    reqDataExists = false;
end

% Attempt to create the requested data
if ~dataFileExists || ~reqDataExists
    % Create the requested data
    dispMsg(' - Attempt to create the requested data', 1, msgMode);

    
    % Go through experiments of a different size
    dispMsg('   - Perform experiments of different sizes to obtain the requested data', 1, msgMode);
    
    % Initialize variables for the calculation
    convFlag = false;
    sumPow = 2;
    while ~convFlag

        for numSamplesPow = 1 : (sumPow - 1);
            numSamples = 10^numSamplesPow;
            numExpsPerSamplePow = sumPow - numSamplesPow;
            numExpsPerSample = 10^numExpsPerSamplePow;
            dispMsg(['     - Consider a computation with 10^', int2str(numSamplesPow), ' samples of 10^', int2str(numExpsPerSamplePow), ' experiments'], 2, msgMode);
            
            dataAlreadyComp = false;
            if dataFileExists
                % Check if the data for the current parameters have been computed already
                dispMsg('       - Check if the data for this experiment have been computed before', 2, msgMode);
                if sum( ismember(availData, [numSamples numExpsPerSample], 'rows') ) > 0
                    % The data for these parameters have been compute
                    % before
                    dispMsg('         - The data for this experiment have been computed before, but do not have the desired level of accuracy', 2, msgMode);
                    dataAlreadyComp = true;
                else
                    % The data for these parameters have not been compute
                    % before
                    dispMsg('         - The data for this experiment have not been computed before', 2, msgMode);
                end
            end
            
            if ~dataAlreadyComp
                % The data has not been computed before, do computation now

                % Perform computation
                dispMsg('       - Perform computation', 2, msgMode)

                % Check if the experiment is likely going to take a lot of
                % computation time.
                if sumPow > 6
                    disp(' * WARNING: This computation can take more than an hour to finish');
                    if msgMode < 2
                        disp('            Rerun in verbose mode ''guesstimateModelCst(.., .., .., 2)'' for more information');
                    end
                end

                tic;
                modDissStatsNew = computeModDissStats(modName, modVisc, numSamples, numExpsPerSample, sampleMode);
                elapsedTime = toc;
                dispMsg(['         - The computation took ', num2str(elapsedTime), ' s'], 2, msgMode);
               
                % Check if new data have the desired level of precision
                dispMsg('       - Check if the newly computed data have the desired level of precision', 2, msgMode);
                curStdDevRel = modDissStatsNew.modDissStdDevRel;

                if curStdDevRel < minRelPrecision / sqrt(2)
                    % The newly computed data have the desired level of
                    % precision
                    dispMsg(['         - The newly computed data have the desired precision: ', num2str(curStdDevRel), ' < minRelPrecision / sqrt(2) = ', num2str(minRelPrecision / sqrt(2))], 2, msgMode);
                    dispMsg('   - The requested data have been created', 1, msgMode);
                    
                    convFlag = true;

                    modDissAvg = modDissStatsNew.modDissAvg;
                    modDissRelStdDev = curStdDevRel;
                    break;
                else
                    % The newly computed data do not have the desired level 
                    % of precision
                    dispMsg(['         - The newly computed data do not have the desired precision as: ', num2str(curStdDevRel), ' >= minRelPrecision / sqrt(2) = ', num2str(minRelPrecision / sqrt(2))], 2, msgMode);
                end
            end

        end

        sumPow = sumPow + 1;
    end
end
    
end