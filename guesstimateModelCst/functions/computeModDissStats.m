function modDissStats = computeModDissStats(modName, modVisc, numSamples, numExpsPerSample, sampleMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Given a (scaled) eddy viscosity model, obtain an estimate of its (scaled)
% dissipation using a number of random traceless velocity gradients.
%
% Note that the scaled eddy viscosity is defined as nu_e / c_1 / delta^2,
% where c_1 represents the model constant (C_S^2 for the Smagorinsky model)
% and delta is the usual large-eddy simulation filter length.
%
% Note that this script can also be called directly to expand the database
% from which model constant estimates can be constructed.
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modName:          string - name of eddy viscosity model
%                   Example input:
%                       'Smagorinsky',
%                       'Vreman'
%                       'QR'
% modVisc:          string - scaled eddy viscosity: nu_e / C^2 / delta^2
%                   Example input:
%                       'sqrt(2*I1)',
%                       'sqrt( ( 1/4*(I1 + I2)^2 + 4*(I5 - 1/2*I1*I2) ) / (I1 - I2) )',
%                       '2/3/3*max(-I3, 0)/I1'
%                   Explanation:
%                       This string can represent any function call 
%                       involving the velocity gradient, denoted G, as long
%                       as it has a scalar output. 
%                       For simplicity, shorthands are available for the 
%                       rate-of-strain tensor, S, 
%                       the rate-of-rotation tensor, W, 
%                       and their combined invariants:
%                       I1 = trace(S*S),
%                       I2 = trace(W*W),
%                       I3 = trace(S*S*S),
%                       I4 = trace(S*W*W),
%                       I5 = trace(S*S*W*W)
% numSamples:       int - number of samples to be constructed
% numExpsPerSample: int - number of experiments per sample
% sampleMode:       int - type of sampling
%                       0: uniform sampling,
%                       1: Gaussian sampling
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modDissStats:     struct - contains all the input and statistical data:
%                   set of means of (scaled) dissipation, and this set:
%                   the mean, variance, standard deviation, and relative
%                   standard deviation,
%                   where the (scaled) dissipation is calculated as 
%                   nu_e * I1 / C^2 / delta^2 = modVisc * I1
%                   Output is stored in dat/modDissStatsDB.mat
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% MIT License
% 
% Copyright (c) 2016 Maurits H. Silvis
% 
% Permission is hereby granted, free of charge, to any person obtaining 
% a copy of this software and associated documentation files (the 
% "Software"), to deal in the Software without restriction, including 
% without limitation the rights to use, copy, modify, merge, publish, 
% distribute, sublicense, and/or sell copies of the Software, and to 
% permit persons to whom the Software is furnished to do so, subject 
% to the following conditions:
% 
% The above copyright notice and this permission notice shall be 
% included in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
% IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
% CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
% TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Obtain numSamples samples of size numExpPerSample of the scaled
    % dissipation and compute their means
    modDissMeans = zeros(numSamples, 1);
    for idxSample = 1 : numSamples
        modDissMeans(idxSample) = mean(computeModDissSample(modVisc, numExpsPerSample, sampleMode));
    end

    % Compute the mean of all the data
    modDissMeansAvg = mean(modDissMeans);

    % Compute the variance of the means
    modDissMeansVar = var(modDissMeans);

    % Compute the standard deviation of the means
    modDissMeansStdDev = sqrt(modDissMeansVar);

    % Compute the relative standard deviation of the means
    modDissMeansStdDevRel = modDissMeansStdDev / modDissMeansAvg;

    % Construct the data structure
    modDissStats = struct( ...
        'modName', modName, ...
        'modVisc', modVisc, ...
        'numSamples', numSamples, ...
        'numExpsPerSample', numExpsPerSample, ...
        'sampleMode', sampleMode, ...
        'sampleModeStr', getSampleModeStr(sampleMode), ...
        'modDissAvgs', modDissMeans, ...
        'modDissAvg', modDissMeansAvg, ...
        'modDissVar', modDissMeansVar, ...
        'modDissStdDev', modDissMeansStdDev, ...
        'modDissStdDevRel', modDissMeansStdDevRel ...
    );

    % Store the data
    saveModDissStats(modDissStats);

end

function [modDissSampleData] = computeModDissSample(modVisc, numExps, sampleMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Given a scaled eddy viscosity, obtain numExps samples of the model's 
% dissipation
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modVisc:              string - scaled eddy viscosity
% numExps:              int - number of experiments
% sampleMode:           int - type of sampling
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modDissSampleData:    1 x numExps matrix - model dissipation samples
%
% Copyright (c) 2016 Maurits H. Silvis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    modDissSampleData = zeros(1, numExps);
    for idxExp = 1 : numExps
        modDissSampleData(idxExp) = computeModDiss(modVisc, sampleMode);
    end

end

function modDiss = computeModDiss(modVisc, sampleMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Given a scaled eddy viscosity, obtain a sample of its scaled dissipation
% for a random traceless velocity gradient
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modVisc:      string - scaled eddy viscosity
% sampleMode:   int - type of sampling
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modDiss:      double - scaled model dissipation sample
%
% Copyright (c) 2016 Maurits H. Silvis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Obtain random traceless velocity gradient
    G = generateRandVelGrad(sampleMode);

    % Compute derived quantities:
    % Rate-of-strain and rate-of-rotation tensors
    S = 1/2*(G + G');
    W = 1/2*(G - G');

    % Compute derived quantities: invariants
    I1 = trace(S*S);
    I2 = trace(W*W);
    I3 = trace(S*S*S);
    I4 = trace(S*W*W);
    I5 = trace(S*S*W*W);
    % I6 = trace(S*S*W*W*S*W)

    % Compute dissipation

    % For the given random velocity field,
    % compute the value of the (scaled) dissipation 
    modDiss = eval(modVisc) * I1;

end

function G = generateRandVelGrad(sampleMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Construct a random traceless 3 x 3 matrix that will serve as a random
% velocity gradient
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% sampleMode:   int - type of sampling
%                   0: uniform, select values uniformly on [a, b]
%                   1: Gaussian, select values from a standard normal 
%                      distribution
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% G:            3 x 3 matrix - random velocity gradient
%
% Copyright (c) 2016 Maurits H. Silvis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if sampleMode == 0
        % Select random numbers uniformly

        % Interval of random numbers: [a, b]
        % TODO: Test dependence on interval
        a = -1;
        b =  1;

        % Generate random matrix
        A = rand(3);

        % Shift to desired interval
        A = a + A*(b - a);
    elseif sampleMode == 1
        % Select random number from a standard normal distribution

        A = normrnd(0, 1, 3, 3);
    end

    % Make traceless
    Adev = A - 1/3*trace(A)*eye(3);

    % Define traceless velocity gradient
    G = Adev;

    % Check trace w.r.t eps
    trG  = trace(G);
    if trG > 10*eps
    %     trG
        sprintf('Warning: the trace of the velocity gradient is more than ten times larger than eps!\ntrG = %.3e > %.3e', trG, 10*eps)
    end
end

function saveModDissStats(modDissStatsCur)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Auxiliary function that store the statistical data in a database
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modDissStats: struct - contains input and statistical data
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Check if the data file already exists
    if exist('dat/modDissStatsDB.mat', 'file') == 2
        % The data file exist
        load('dat/modDissStatsDB', 'modDissStats')
        numExps = numel(modDissStats);
    else
        % The data file doesn't exist
        
        % Check if the folder exists
        if exist('dat', 'dir') == 0
            % The folder doesn't exist, create it
            mkdir('dat');
        end
        numExps = 0;
    end
    
    numExps = numExps + 1;
    
    modDissStats(numExps) = modDissStatsCur;
    
    % Sort the data by model name, sampling mode and the variance of the set of means
    [~,index] = sortrows({modDissStats.modDissVar}.'); modDissStats = modDissStats(index); clear index
    [~,index] = sortrows({modDissStats.sampleMode}.'); modDissStats = modDissStats(index); clear index
    [~,index] = sortrows({modDissStats.modName}.'); modDissStats = modDissStats(index); clear index
       
    save('dat/modDissStatsDB', 'modDissStats');
    
end