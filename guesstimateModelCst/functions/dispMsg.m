function dispMsg(msgStr, msgLvl, msgMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Auxiliary function that display a message or not depending on the chosen 
% mode
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% msgStr:   string - message string
% msgLvl:   int - message level
% msgMode:  int - message mode
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (c) 2016 Maurits H. Silvis
%
% This file is subject to the terms and conditions defined in 
% the MIT License, which can be found in the file 'LICENSE.txt' 
% that is part of this source code package.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if msgMode >= msgLvl
       disp(msgStr); 
    end
    
end