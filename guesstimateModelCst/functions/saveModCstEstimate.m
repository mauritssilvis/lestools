function saveModCstEstimate(modCstEstimate)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Auxiliary function that stores the obtained data in a database
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modCstEstimate:   struct - contains input data and estimates
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (c) 2016 Maurits H. Silvis
%
% This file is subject to the terms and conditions defined in 
% the MIT License, which can be found in the file 'LICENSE.txt' 
% that is part of this source code package.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    % Check if the data file exists
    if exist('dat/modCstEstimatesDB.mat', 'file') == 2
        % The data file exist
        load('dat/modCstEstimatesDB', 'modCstEstimates');
        dataFileExists = true;
        numExps = numel(modCstEstimates);
    else
        % The data file doesn't exist
        dataFileExists = false;
        
        % Check if the folder exists
        if exist('dat', 'dir') == 0
            % The folder doesn't exist, create it
            mkdir('dat');
        end
        numExps = 0;
    end
    
    dataStored = false;
    if dataFileExists
        % Check if the current data were computed and stored before
        for idxExp = 1 : numExps
            curModName = modCstEstimates(idxExp).modName;
            curSampleMode = modCstEstimates(idxExp).sampleMode;
            curModCstEstimate = modCstEstimates(idxExp).modCstEstimate;
            curModCstRelStdDev = modCstEstimates(idxExp).modCstRelStdDev;
            
            if strcmp(modCstEstimate.modName, curModName) && modCstEstimate.sampleMode == curSampleMode && modCstEstimate.modCstEstimate == curModCstEstimate && modCstEstimate.modCstRelStdDev == curModCstRelStdDev
                % The data have been computed and stored before
                dataStored = true;
                break;
            end                
        end
    end
    
    if ~dataStored
        % Store the data, as they are new
        numExps = numExps + 1;
    
        modCstEstimates(numExps) = modCstEstimate;
        
        % Sort the data by model name, sampling mode and the relative standard devation
        [~,index] = sortrows({modCstEstimates.modCstRelStdDev}.'); modCstEstimates = modCstEstimates(index); clear index
        [~,index] = sortrows({modCstEstimates.sampleMode}.'); modCstEstimates = modCstEstimates(index); clear index
        [~,index] = sortrows({modCstEstimates.modName}.'); modCstEstimates = modCstEstimates(index); clear index
       
        save('dat/modCstEstimatesDB', 'modCstEstimates');
    end
    
end