function modCstEstimate = guesstimateModelCst(modName, modVisc, minRelPrecision, msgMode)

% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Obtain an estimate of the constant of an eddy viscosity model for large-
% eddy simulation of turbulent flows by requiring that its average 
% dissipation matches the dissipation of the Smagorinsky model for a large 
% number of random velocity gradients, sampled from a uniform or a Gaussian 
% distribution.
%
% This is implemented by computing the ratio of the estimated average 
% (scaled) dissipation due to the Smagorinsky model and the estimated 
% average (scaled) dissipation of the model of interest, where averages are
% taken over a large number of random velocity gradients. As a result one 
% obtains an estimate of the value of the model constant, c_1, in terms of 
% the Smagorinsky model constant (squared), C_S^2.
%
% Note that the scaled dissipation for the Smagorinsky model is defined as
% sqrt(2*I1) * I1 / C_S^2 / delta^2, where sqrt(2*I1) is called the scaled 
% eddy viscosity. Here, I1 is the second invariant of the rate-of-strain
% tensor and delta is the large-eddy simulation filter length, usually 
% associated with the grid spacing. In this script, delta is assumed to be 
% implemented in the same fashion in the Smagorinsky model and the model of
% interest, so it can be divided out in the ratio of the average
% dissipations.
%
% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modName:          string - name of eddy viscosity model
%                   Example input:
%                       'Vreman'
%                       'QR'
% modVisc:          string - scaled eddy viscosity: nu_e / C^2 / delta^2
%                   Example input:
%                       'sqrt( ( 1/4*(I1 + I2)^2 + 4*(I5 - 1/2*I1*I2) ) / (I1 - I2) )',
%                       '2/3/3*max(-I3, 0)/I1'
%                   Explanation:
%                       This string can represent any function call 
%                       involving the velocity gradient, denoted G, as long
%                       as it has a scalar output. 
%                       For simplicity, shorthands are available for the 
%                       rate-of-strain tensor, S, 
%                       the rate-of-rotation tensor, W, 
%                       and their combined invariants:
%                       I1 = trace(S*S),
%                       I2 = trace(W*W),
%                       I3 = trace(S*S*S),
%                       I4 = trace(S*W*W),
%                       I5 = trace(S*S*W*W)
% minRelPrecision:  double - desired minimal relative standard deviation of
%                   the estimate of the model constant
%                   Example input:
%                       0.01,
%                       0.005 
% msgMode:          int - switch that controls the output
%                   Explanation:
%                       Control the amount out text output to the screen.
%                       0: silent mode, all messages apart from warnings
%                          are suppresed,
%                       1: default mode, some but not all status messages
%                          are shown,
%                       2: verbose mode, all status messages are shown
%
% OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% modCstEstimate:   struct - contains input data modName and modVisc, and
%                   variables:
%                       sampleMode:      0 for uniform sampling,
%                                        1 for Gaussian sampling,
%                       modCstEstimate:  estimate of model constant c_1,
%                       modCstRelStdDev: estimate of its relative error
%                   Output is stored in dat/modCstEstimatesDB.mat
%
% LICENSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% MIT License
% 
% Copyright (c) 2016 Maurits H. Silvis
% 
% Permission is hereby granted, free of charge, to any person obtaining 
% a copy of this software and associated documentation files (the 
% "Software"), to deal in the Software without restriction, including 
% without limitation the rights to use, copy, modify, merge, publish, 
% distribute, sublicense, and/or sell copies of the Software, and to 
% permit persons to whom the Software is furnished to do so, subject 
% to the following conditions:
% 
% The above copyright notice and this permission notice shall be 
% included in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
% EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
% IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
% CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
% TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Check input
    if nargin < 4
        % msgMode is not specified
        msgMode = 1;

        % Check if other inputs are specified
        if nargin < 3
            % minRelPrecision and possibly other input arguments are not specified
            disp('* WARNING: Not enough input arguments were specified');
            disp('           Type ''help guesstimateModelCst'' for more information'); 
            return;
        end
    else
        % msgMode is specified, check its value
        if msgMode ~= 0 && msgMode ~= 2
            % Set the default value
            msgMode = 1;
        end
    end

    % Add path to auxiliary functions
    addpath('functions');

    % Obtain estimates of the eddy viscosity's model constant for each of the 
    % sampling modes:
    % 0: uniform sampling
    % 1: Gaussian sampling
    dispMsg(['Obtain estimates of the constant of the model of interest (', modName, ')'], 1, msgMode);
    dispMsg('in terms of the Smagorinsky model constant', 1, msgMode);

    for sampleMode = 0 : 1
        % Obtain reference data for the Smagorinsky model
        dispMsg([' * Obtain reference data for the Smagorinsky model with ', getSampleModeStr(sampleMode), ' sampling'], 1, msgMode);
        [refDissAvg, refDissRelStdDev] = obtainModDissStats('Smagorinsky', 'sqrt(2*I1)', sampleMode, minRelPrecision, msgMode);

        % Obtain data for the model of interest
        dispMsg([' * Obtain data for the model of interest (', modName,') with ', getSampleModeStr(sampleMode), ' sampling'], 1, msgMode);
        [modDissAvg, modDissRelStdDev] = obtainModDissStats(modName, modVisc, sampleMode, minRelPrecision, msgMode);


        % Construct the data structure    
        modCstEstimate(sampleMode + 1) = struct( ...
            'modName', modName, ...
            'modVisc', modVisc, ...
            'sampleMode', sampleMode, ...
            'sampleModeStr', getSampleModeStr(sampleMode), ...
            'modCstEstimate', refDissAvg / modDissAvg, ...
            'modCstRelStdDev', sqrt(refDissRelStdDev^2 + modDissRelStdDev^2) ...
        );

        % Display the result
        dispMsg(['The model constant for the model of interest (', modName, ') is estimated to be'], 1, msgMode);
        dispMsg(['   c_1 / C_S^2 = ', num2str(modCstEstimate(sampleMode + 1).modCstEstimate), ' ', char(177), ' ', num2str(modCstEstimate(sampleMode + 1).modCstRelStdDev*100), ' %'], 1, msgMode);
        dispMsg(['using ', getSampleModeStr(sampleMode), ' sampling'], 1, msgMode);

        % Store the data
        saveModCstEstimate( modCstEstimate(sampleMode + 1) );

    end

end